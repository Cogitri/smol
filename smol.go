/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"text/template"

	"github.com/mcuadros/go-version"
	"github.com/tidwall/gjson"
)

type packageInfo struct {
	PackageName string
	SubRepo string
	UpstreamVersion string
	ExherboVersion string
}

// Get the API-Response and unmarshal the JSON-Data
func readJSONData(firstPackage string) (decodedJSONData map[string]interface{}, apiResponse []byte) {
	response, err := http.Get("https://repology.org/api/v1/metapackages/" + firstPackage + "/?inrepo=exherbo&outdated=1")

	if err != nil {
		log.Fatalf("The HTTP request failed with error %s\n", err)
	} else {
		apiResponse, err = ioutil.ReadAll(response.Body)
	}

	if err := json.Unmarshal(apiResponse, &decodedJSONData); err != nil {
		log.Fatalf("The decoding of the JSON failed with error %s\n", err)
	}

	return decodedJSONData, apiResponse
}

func getExherboRepo(packageName string, apiResponse []byte) (exherboSubRepoNormalized string) {
	// We want to get the subrepo the package resides in.
	// For this to work we need to get the contents of the jsonSubRepo
	// entry for the entry of that package that has 'repo=exherbo'
	jsonSubRepo := packageName + `.#[repo="exherbo"].subrepo`
	exherboSubRepo := gjson.Get(string(apiResponse[:]), jsonSubRepo)

	// We don't want dev/<repo> for dev repositories (I suppose?)
	if strings.Contains(exherboSubRepo.String(), "/") {
		exherboSubRepoNormalized = strings.Split(exherboSubRepo.String(), "/")[1]
	} else {
		exherboSubRepoNormalized = exherboSubRepo.String()
	}

	return exherboSubRepoNormalized
}

func comparePackageVersion(decodedJSONData map[string]interface{}, apiResponse []byte) (packageName string, packageNameSlice []string, upstreamVersionSlice []string, exherboVersionSlice []string, exherboSubRepoSlice []string) {
	for packageName = range decodedJSONData {
		// Repology saves in the json file like this:
		// $package [
		// {
		// version: <version>,
		// status: <status>,
		// ]
		// }
		jsonVersion := string(packageName + `.#[status="newest"].version`)
		upstreamVersion := gjson.Get(string(apiResponse[:]), jsonVersion).String()

		jsonExherboVersion := string(packageName + `.#[repo="exherbo"].version`)
		exherboVersion := gjson.Get(string(apiResponse[:]), jsonExherboVersion).String()

		exherboSubRepo := getExherboRepo(packageName, apiResponse)

		// if upstream has a newer package we add the package's info to the slice, which is later
		// written to a html file
		if version.Compare(version.Normalize(upstreamVersion), version.Normalize(exherboVersion), ">") {
			packageNameSlice = append(packageNameSlice, packageName)
			upstreamVersionSlice = append(upstreamVersionSlice, upstreamVersion)
			exherboVersionSlice = append(exherboVersionSlice, exherboVersion)
			exherboSubRepoSlice = append(exherboSubRepoSlice, exherboSubRepo)
		}

	}

	return packageName, packageNameSlice, upstreamVersionSlice, exherboVersionSlice, exherboSubRepoSlice
}

func getNextPage(packageName string) (nextItem string) {
	nextItem = packageName
	return nextItem
}

// TODO: Add pagenumbers to the html files
func generateHTML(htmlData []packageInfo, pageNumber int) (returnError error) {
	// read layout.html (the template)
	tmpl := template.Must(template.ParseFiles("layout.html"))
	// write html file thos this location
	htmlFile, err := os.Create("./html/" + strconv.Itoa(pageNumber) + ".html")
	if err != nil {
		returnError = errors.New("Couldn't creat file")
	}
	// merge data and template
	err = tmpl.Execute(htmlFile, htmlData)
	if err != nil {
		returnError = errors.New("Couldn't write template")
		fmt.Println(err)
	}
	htmlFile.Close()

	return returnError
}

func main() {
	decodedJSONData, apiResponse := readJSONData("")
	packageName, packageNameSlice, upstreamVersionSlice, exherboVersionSlice, exherboSubRepoSlice := comparePackageVersion(decodedJSONData, apiResponse)
	var nextItem string

	pageNumber := 1

	htmlData := make([]packageInfo, 0)

	// Because repology only delivers a limited set of packages at a time
	// we have to loop over this
	for {
		nextItem = getNextPage(packageName)

		// Add all data to a map
		for i := range packageNameSlice {
			htmlData = append(htmlData, packageInfo{PackageName: packageNameSlice[i], SubRepo: exherboSubRepoSlice[i], UpstreamVersion: upstreamVersionSlice[i], ExherboVersion: exherboVersionSlice[i]})
		}

		decodedJSONData, apiResponse = readJSONData(string(nextItem + "/"))
		packageName, packageNameSlice, upstreamVersionSlice, exherboVersionSlice, exherboSubRepoSlice = comparePackageVersion(decodedJSONData, apiResponse)

		// If we reached the end of the list we should break
		if nextItem == packageName {
			break
		}

		nextItem = getNextPage(nextItem)
	}

	generateHTML(htmlData, pageNumber)

	fmt.Println("Sucessfully determined all outdated packages")

}

package main

import (
	"testing"

	"github.com/tidwall/gjson"
)

func TestReadJSONData(t *testing.T) {
	_, apiResponse := readJSONData("")

	subrepo := gjson.Get(string(apiResponse[:]), `0ad.#[repo="exherbo"].subrepo`)

	possibleRepos := [28]string{"arbor", "bikeshed", "desktop", "emacs", "enlightment", "games", "gnome", "hardware", "haskell", "java", "kde", "lisp", "media", "mono", "net", "office", "perl", "php", "python", "ruby", "rust", "scientific", "selinux", "tcl", "vim", "virtualization", "x11", "xfce"}

	success := false

	for i := range possibleRepos {
		if possibleRepos[i] == subrepo.String() {
			success = true
			break
		}
	}

	if !success {
		t.Errorf("Couldn't Download and parse API response")
	}
}

func TestGenerateHTML(t *testing.T) {
	testSlice := make([]packageInfo, 0)

	testNameArr := [8]string{"cdrdao", "db6", "cgal", "dejagnu", "aircrack-ng", "crossguid", "ccid", "clinfo"}
	testUpstreamVersion := [8]string{"1.2.4", "6.2.32", "4.12", "1.6.1", "1.3", "0.2.2", "1.4.28", "2.2.17"}
	testExherboVersion := [8]string{"1.2.3", "6.2.31", "4.11", "1.6.0", "1.2", "0.2", "1.3_p1", "2.1-rc2"}
	testExherboSubrepo := [8]string{"arbor", "games", "net", "dev/cogitri", "dev/keruspe", "dev/heirecka", "selinux", "bikeshed"}

	for i := range testNameArr {
		testSlice = append(testSlice, packageInfo{PackageName: testNameArr[i], SubRepo: testExherboSubrepo[i], UpstreamVersion: testUpstreamVersion[i], ExherboVersion: testExherboVersion[i]})
	}

	fmt.Println(testSlice)

	err := generateHTML(testSlice, 11111)

	if err != nil {
		t.Errorf("Couldn't write HTML template! The error is the following: %s", err)
	}

}

PROJECT_NAME := "smol"
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all build clean

all: build

build: build_dep
	@go build -i -v $(GO_FILES)

build_dep:
	@go get -v -d

clean:
	@rm -f $(PROJECT_NAME)

install:
	@go install -v

lint: test_dep
	@golint -set_exit_status $(GO_FILES)

test_dep:
	@go get -u github.com/golang/lint/golint # required for linting later on

check: test_dep
	@go test -v